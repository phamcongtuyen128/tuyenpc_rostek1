
(cl:in-package :asdf)

(defsystem "beginner_tutorials-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "scan_range" :depends-on ("_package_scan_range"))
    (:file "_package_scan_range" :depends-on ("_package"))
  ))