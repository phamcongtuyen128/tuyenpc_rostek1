# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/workspace/src/turtle/turtle/turtlebot3/turtlebot3_slam/include".split(';') if "/workspace/src/turtle/turtle/turtlebot3/turtlebot3_slam/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;sensor_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "turtlebot3_slam"
PROJECT_SPACE_DIR = "/workspace/devel"
PROJECT_VERSION = "1.2.4"
