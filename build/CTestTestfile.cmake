# CMake generated Testfile for 
# Source directory: /workspace/src
# Build directory: /workspace/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("beginner_tutorials")
subdirs("student_scan_matcher")
subdirs("f1tenth_simulator")
